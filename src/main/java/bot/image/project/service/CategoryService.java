package bot.image.project.service;

import bot.image.project.interfaces.repository.CategoryRepository;
import bot.image.project.interfaces.service.ICategory;
import bot.image.project.model.Category;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService implements ICategory {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public void addCategory(Category category) {

    }

    public List<Category> getAllCategoriesFromWeb(String url) {
        try {
            List<Category> categories = new ArrayList<>();
            Document doc = Jsoup.connect(url).get();

            Elements body = doc.select("body");
            Element container = body.select("div.container").first();
            Element container_right = container.selectFirst("div.container_right");
            Element container_inside = container_right.selectFirst("div.container_inside");
            Element content_background = container_inside.selectFirst("div.content_background");
            Element left_panel = content_background.selectFirst("div.left-panel");
            Element sidebox_categories = left_panel.selectFirst("div");
            Element sidebox_body_r = sidebox_categories.selectFirst("div.sidebox-body-r");
            Element sidebox_body_l = sidebox_body_r.selectFirst("div.sidebox-body-l");
            Element sidebox_body_c = sidebox_body_l.selectFirst("div.sidebox-body-c");
            Element sidebox_body_cnt = sidebox_body_c.selectFirst("div.sidebox-body-cnt");
            Element ul = sidebox_body_cnt.selectFirst("ul");
            Elements li = ul.select("li");
            for(int i = 1; i < li.size()-1; i++) {
                String categoryName = li.get(i).select("a").text();
                categories.add(new Category(Long.valueOf(i), categoryName));
            }

            return categories;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
