package bot.image.project.interfaces.service;

import bot.image.project.model.Image;

import java.net.URL;
import java.util.List;

public interface IImage {

    List<Image> getAllImages();

    byte[] getOneImage(URL url);
}
