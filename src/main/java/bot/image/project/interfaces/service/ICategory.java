package bot.image.project.interfaces.service;

import bot.image.project.model.Category;

import java.util.List;

public interface ICategory {

    List<Category> getAllCategories();

    void addCategory(Category category);
}
