package bot.image.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageBotApplication.class, args);
	}

}
